FROM ruby
RUN apt-get update -q && apt-get install -y nodejs
# build-essential libpq-dev

RUN mkdir /myapp
ADD . /myapp
WORKDIR /myapp

# ADD Gemfile /myapp/Gemfile
# ADD Gemfile.lock /myapp/Gemfile.lock

RUN bundle install
# RUN bin/setup
